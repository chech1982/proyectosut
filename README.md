
¿Sabes qué es Sistema Único de Trámites? 

Es una herramienta informática para la elaboración, simplificación y estandarización del Texto Único de Procedimientos Administrativos (TUPA), y también es el repositorio oficial de los procedimientos administrativos y servicios prestados en exclusividad por las entidades del Estado. Fue creado mediante Decreto Legislativo 1203

El SUT une los esfuerzos de la Secretaría de Gestión Pública y de las entidades de la administración pública para mejorar la Simplificación Administrativa y el cumplimiento de la Ley de Transparencia, mediante la gestión integral de los TUPA.


### Main features to come:
- Fast 2D rendering (UI, particles, sprites, etc.)
- High-fidelity Physically-Based 3D rendering (this will be expanded later, 2D to come first)
- Support for Mac, Linux, Android and iOS
    - Native rendering API support (DirectX, Vulkan, Metal)
- Fully featured viewer and editor applications
- Fully scripted interaction and behavior
- Integrated 3rd party 2D and 3D physics engine
- Procedural terrain and world generation
- Artificial Intelligence
- Audio system